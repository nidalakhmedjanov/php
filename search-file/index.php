<?php

$searchRoot = 'C:\Users\nidal\OneDrive\Desktop\work\Skilbox\PHP\php\search-file';
$searchName = 'test.txt';
$searchResult = [];

function searchFile ( string $searchRoot, string $searchName, array &$searchResult)
{
  array_map(function (string $item) use ($searchRoot, $searchName, &$searchResult) {
    if ($item === '.' || $item === '..') {
      return;
    } elseif (is_dir("$searchRoot\\$item")) {
      searchFile("$searchRoot\\$item", $searchName, $searchResult);
    } elseif ($item === $searchName) {
      $searchResult[] = "$searchRoot\\$searchName";
    }
  }, scandir($searchRoot));
}

function notNullSizeFilesFilter (array $filesPath): array
{
  return array_filter($filesPath, fn (string $item) => filesize($item) > 0);
}

searchFile($searchRoot, $searchName, $searchResult);

echo 'Все найденные файлы: ' . PHP_EOL;
print_r($searchResult);

echo 'Найденные файлы размер которых не равен нулю: ' . PHP_EOL;
print_r(notNullSizeFilesFilter($searchResult));
